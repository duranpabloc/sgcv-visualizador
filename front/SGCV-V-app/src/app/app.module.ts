import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TemplateVanillaComponent } from './template-vanilla/template-vanilla.component';
import { VideoComponent } from './video/video.component';
import { AdsComponent } from './ads/ads.component';
import { NewsComponent } from './news/news.component';
import { DateComponent } from './date/date.component';

@NgModule({
  declarations: [
    AppComponent,
    TemplateVanillaComponent,
    VideoComponent,
    AdsComponent,
    NewsComponent,
    DateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
