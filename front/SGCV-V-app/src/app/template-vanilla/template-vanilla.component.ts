import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-template-vanilla',
  templateUrl: './template-vanilla.component.html',
  styleUrls: ['./template-vanilla.component.css']
})
export class TemplateVanillaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
