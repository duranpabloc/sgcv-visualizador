import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateVanillaComponent } from './template-vanilla.component';

describe('TemplateVanillaComponent', () => {
  let component: TemplateVanillaComponent;
  let fixture: ComponentFixture<TemplateVanillaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplateVanillaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateVanillaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
